""" 
   example use: arch -i386 /usr/bin/python2.7 bookClubBot.py <Group Chat Name>
"""

from Skype4Py import Skype
from functools import partial
import Skype4Py
import sys
import re


#Set up client to be attached to skype
chatName = ' '.join(sys.argv[1:])
debug = True

class GroupChat:
  def __init__(self, topic, messageProcessing):
    #attach this object to skype
    self.client = Skype(Events=self)
    self.client.Attach()

    #find the chat with the specified topic
    self.chat = self.findChat(topic)
    #assign the function that you want to use to process the message
    self.processMessage = messageProcessing

  #find the chat with topic set to the var topic
  def findChat(self, topic):
    for chat in self.client.Chats:
      if chat.Topic == topic:
        return chat

  #this message is called every time a message's status changes within skype
  #this could be a new message is sent/received or that a message is finally read
  # + some more stuff
  def MessageStatus(self, msg, status):
    if msg.Chat == self.chat:
      response = self.processMessage(self, msg, status)
      if response != '' and response != None:
        self.chat.SendMessage(response)
        

############
##  PRINT ##
############
def printMessage(group, msgObj, status):
  print(msgObj.Body)

################
##  Book Club ##
################

week = 8
books = [('DesignPatterns.book', 'Head First Design Patterns', 13), 
	 ('ConcurrencyInJava.book', 'Java Concurrency in Practice', 15)]

def rotationIndex(group):
  return week % len(group.chat.Members)

def prefixSkypeBot(f, group, msg):
  return '[SkypeBot]: ' + f(group, msg)

def prefixMeSkypeBot(f, group, msg):
  return '/me [SkypeBot]: ' + f(group, msg)

#######################################
## Processing for Book Club Commands ##
#######################################

def getHelpString(group, msg):
  return 'Commands: ' + (', '.join(commands))

def getLeader(group, msg):
  output = group.chat.Members[rotationIndex(group)].FullName + " is the leader"
  return output

def getRotation(group, msg):
  rotationWithLeader = []
  for i in range(0, len(group.chat.Members)):
    if i == rotationIndex(group):
      rotationWithLeader.append('(' + group.chat.Members[i].FullName + ')')
    else:
      rotationWithLeader.append(group.chat.Members[i].FullName)
  output = ', '.join(rotationWithLeader)
  return output

def getBook(group, msg):
  book, title, x = currentBookWithRemainder()
  return title 

def getChapter(group, msg):
  book, title, offset = currentBookWithRemainder()
  f = open(book)
  cnt = 1
  for line in f:
    if cnt == offset:
      return line
    else:
      cnt += 1

def currentBookWithRemainder():
  offset = week 
  for (book, title, lines) in books:
    if offset <= lines:
      return (book, title, offset)
    else:
      offset -= lines

#A dict that holds tuples of (command string, processing for that command string)
#to add a command, add the command string and processing to this dict
commandsWithProc = {
		'help': partial(prefixSkypeBot, getHelpString), 
                'leader': partial(prefixMeSkypeBot, getLeader), 
		'rotation': partial(prefixMeSkypeBot, getRotation),
		'chapter' : partial(prefixMeSkypeBot, getChapter),
		'book' : partial(prefixMeSkypeBot, getBook)
                }
commands = commandsWithProc.keys()

def isCommand(msg):
  return msg in commands

def bookClubProcStart(group, msgObj, status):
  if isCommand(msgObj.Body.lower()):
    return (commandsWithProc[msgObj.Body.lower()](group, msgObj.Body))

########################################
## Code to only process messages once ##
########################################

#A set to hold recent messages that have not been read yet so we don't process them 2x
recentMessages = set()

#Higher level function to only process messages that are being sent/received
def OnlyProcessReceivedOrSent(proc, group, msgObj, status):
  if status == Skype4Py.cmsSent:
    return proc(group, msgObj, status)
  elif status == Skype4Py.cmsReceived:
    recentMessages.add(msgObj)
    return proc(group, msgObj, status)
  elif status == Skype4Py.cmsRead:
    #if skype is in the foreground while a message is recieved, the message
    #is directly considered Read, and therefore needs to be processed here
    if not msgObj in recentMessages:
      return proc(group, msgObj, status)
    else:
      recentMessages.remove(msgObj)

#partially apply OnlyprocessRecievedOrSent to bookClubProcStart to get the 
#function that we want the group chat to use to process messages
group = GroupChat(chatName, partial(OnlyProcessReceivedOrSent, bookClubProcStart))

#Run until Ctrl-C is received from command line
try:
  while(True):
    userInput = raw_input("Enter 'update' to update to the next week.\n")
    if userInput.lower() == 'update':
      week += 1
      print("week successfully updated")
except KeyboardInterrupt:
  pass
